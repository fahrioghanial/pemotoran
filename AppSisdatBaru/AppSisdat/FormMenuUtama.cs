﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppSisdat
{
    public partial class FormMenuUtama : Form
    {
        
        public static FormMenuUtama menu;
        MenuStrip mnstrip;
        FormLogin frmLogin;
        void frmLogin_formClosed(object sender, FormClosedEventArgs e)
        {
            frmLogin = null;
        }

        FormEmployee frmEmply;
        void frmEmply_formClosed(object sender, FormClosedEventArgs e)
        {
            frmEmply = null;
        }

        FormCustomer frmCstmr;
        void frmCstmr_formClosed(object sender, FormClosedEventArgs e)
        {
            frmCstmr = null;
        }

        FormAbout frmAbt;
        void frmAbt_formClosed(object sender, FormClosedEventArgs e)
        {
            frmAbt = null;
        }

        FormModel frmModel;
        void frmModel_formClosed(object sender, FormClosedEventArgs e)
        {
            frmModel = null;
        }

        FormCustomerOrder frmCstmrOrd;
        void frmCstmrOrd_formClosed(object sender, FormClosedEventArgs e)
        {
            frmCstmrOrd = null;
        }

        void MenuTerkunci()
        {
            menuLogin.Enabled = true;
            menuLogout.Enabled = false;
            menuEmployee.Enabled = false;
            menuCustomer.Enabled = false;
            menuPurchase.Enabled = false;
            menuModel.Enabled = false;
            menuCustomerOrder.Enabled = false;
            menuUtility.Enabled = false;
            menu = this;
        }
        public FormMenuUtama()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void menuLogin_Click(object sender, EventArgs e)
        {
             if (frmLogin == null)
            {
                frmLogin = new FormLogin();
                frmLogin.FormClosed += new FormClosedEventHandler(frmLogin_formClosed);
                frmLogin.ShowDialog();
            }
            else
            {
                frmLogin.Activate();
            }
        }

        private void FormMenuUtama_Load(object sender, EventArgs e)
        {
            MenuTerkunci();
        }

        private void menuLogout_Click(object sender, EventArgs e)
        {
            MenuTerkunci();
        }

        private void menuMaster_Click(object sender, EventArgs e)
        {
            if (frmEmply == null)
            {
                frmEmply = new FormEmployee();
                frmEmply.FormClosed += new FormClosedEventHandler(frmEmply_formClosed);
                frmEmply.ShowDialog();
            }
            else
            {
                frmEmply.Activate();
            }
        }

        private void menuCustomer_Click(object sender, EventArgs e)
        {
            if (frmCstmr == null)
            {
                frmCstmr = new FormCustomer();
                frmCstmr.FormClosed += new FormClosedEventHandler(frmCstmr_formClosed);
                frmCstmr.ShowDialog();
            }
            else
            {
                frmCstmr.Activate();
            }
        }

        private void gantiPasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (frmAbt == null)
            {
                frmAbt = new FormAbout();
                frmAbt.FormClosed += new FormClosedEventHandler(frmAbt_formClosed);
                frmAbt.ShowDialog();
            }
            else
            {
                frmAbt.Activate();
            }
        }

        private void menuModel_Click(object sender, EventArgs e)
        {
            if (frmModel == null)
            {
                frmModel = new FormModel();
                frmModel.FormClosed += new FormClosedEventHandler(frmModel_formClosed);
                frmModel.ShowDialog();
            }
            else
            {
                frmModel.Activate();
            }
        }

        private void menuCustomerOrder_Click(object sender, EventArgs e)
        {
            if (frmCstmrOrd == null)
            {
                frmCstmrOrd = new FormCustomerOrder();
                frmCstmrOrd.FormClosed += new FormClosedEventHandler(frmCstmrOrd_formClosed);
                frmCstmrOrd.ShowDialog();
            }
            else
            {
                frmCstmrOrd.Activate();
            }
        }
    }
}
